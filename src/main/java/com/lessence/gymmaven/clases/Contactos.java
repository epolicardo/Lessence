/*
 * Copyright 2020 Emiliano Policardo.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lessence.gymmaven.clases;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;

/**
 *
 * @author Emiliano Policardo
 */
public class Contactos {
    
    private int idContacto;
    private Date fechaAlta;
    private Date fechaBaja;
    private Estados Estado;
    private Personas persona;
    private List<Contactos> socios = new ArrayList<>();
    

    public Contactos() {
      
    }

    public List<Contactos> getSocios() {
        return socios;
    }

    public void setSocios(List<Contactos> socios) {
        this.socios = socios;
    }

    public int getIdContacto() {
        return idContacto;
    }

    public void setIdContacto(int idContacto) {
        this.idContacto = idContacto;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Date getFechaBaja() {
        return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
        this.fechaBaja = fechaBaja;
    }

    public Estados getEstado() {
        return Estado;
    }

    public void setEstado(Estados Estado) {
        this.Estado = Estado;
    }

    public Personas getPersona() {
        return persona;
    }

    public void setPersona(Personas persona) {
        this.persona = persona;
    }
    
    public void addSocio(Contactos contacto) {
        this.socios.add(contacto);
        contacto.addSocio(this);
    }
    public static List<Contactos> consultaBase() {
        Session sesion = HibernateUtil.getSessionFactory().openSession();
        Criteria consulta = sesion.createCriteria(Contactos.class);
        List<Contactos> lista = consulta.list();
        sesion.close();
        return lista;
    }
}
